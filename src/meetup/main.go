package main

import (
	"encoding/json"
	"github.com/gorilla/feeds"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

const (
	MAX_PAGE            = "128"
	ORDER               = "time"
	PHOTO_HOST          = "secure"
	TOPIC_CATEGORY_TECH = "292" // tech
	UPCOMING_EVENTS_URL = "https://api.meetup.com/find/upcoming_events"
	FEED_OUTPUT         = "feeds/meetup/upcoming_tech_events_tokyo.xml"
	FEED_URL            = "https://matsuu.gitlab.io/feeds/meetup/upcoming_tech_events_tokyo.xml"
	FEED_TITLE          = "meetup.com Upcoming Tech Events around Tokyo"
	FEED_DESCRIPTION    = "meetup.com Upcoming Tech Events around Tokyo"
)

type jsonTime time.Time

func (t *jsonTime) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(*t).Unix(), 10)), nil
}

func (t *jsonTime) UnmarshalJSON(s []byte) error {
	q, err := strconv.ParseInt(string(s), 10, 64)
	*(*time.Time)(t) = time.Unix(q/1000, q%1000*1000)
	return err
}

func (t *jsonTime) Format(s string) string {
	return time.Time(*t).Format(s)
}

func (t *jsonTime) Time() time.Time {
	return time.Time(*t)
}

type Response struct {
	City   City    `json:"city"`
	Events []Event `json:"events"`
}

type City struct {
	City        string  `json:"city"`
	Country     string  `json:"country"`
	Id          int64   `json:"id"`
	Lat         float64 `json:"lat"`
	Lon         float64 `json:"lon"`
	MemberCount int64   `json:"member_count"`
	State       string  `json:"state"`
	Zip         string  `json:"zip"`
}

type Event struct {
	Created               jsonTime `json:"created"`
	Description           string   `json:"description"`
	Duration              int64    `json:"duration"`
	Fee                   *Fee     `json:"fee,omitempty"`
	Group                 Group    `json:"group"`
	HowToFindUs           string   `json:"how_to_find_us,omitempty"`
	Id                    string   `json:"id"`
	Link                  string   `json:"link"`
	LocalDate             string   `json:"local_date"` // date
	LocalTime             string   `json:"local_time"` // time
	ManualAttendanceCount *int64   `json:"manual_attendance_count,omitempty"`
	Name                  string   `json:"name"`
	ProIsEmailShared      *bool    `json:"pro_is_email_shared,omitempty"`
	RsvpCloseOffset       string   `json:"rsvp_close_offset,omitempty"`
	RsvpLimit             int64    `json:"rsvp_limit,omitempty"`
	RsvpOpenOffset        string   `json:"rsvp_open_offset,omitempty"`
	Status                string   `json:"status"`
	Time                  jsonTime `json:"time"`
	Updated               jsonTime `json:"updated"`
	UtcOffset             int64    `json:"utc_offset"`
	Venue                 *Venue   `json:"venue,omitempty"`
	Visibility            string   `json:"visibility"`
	WaitlistCount         int64    `json:"waitlist_count"`
	YesRsvpCount          int64    `json:"yes_rsvp_count"`
}

type Fee struct {
	Accepts     string  `json:"accepts"`
	Amount      float64 `json:"amount"`
	Currency    string  `json:"currency"`
	Description string  `json:"description"`
	Label       string  `json:"label"`
	Required    bool    `json:"required"`
}
type Group struct {
	Created           jsonTime `json:"created"`
	Id                int64    `json:"id"`
	JoinMode          string   `json:"join_mode"`
	Lat               float64  `json:"lat"`
	LocalizedLocation string   `json:"localized_location"`
	Lon               float64  `json:"lon"`
	Name              string   `json:"name"`
	Region            string   `json:"region"`
	Urlname           string   `json:"urlname"`
	Who               string   `json:"who"`
}

type Venue struct {
	Address1             string  `json:"address_1"`
	Address2             string  `json:"address_2,omitempty"`
	City                 string  `json:"city"`
	Country              string  `json:"country"`
	Id                   int64   `json:"id"`
	Lat                  float64 `json:"lat"`
	LocalizedCountryName string  `json:"localized_country_name"`
	Lon                  float64 `json:"lon"`
	Name                 string  `json:"name"`
	Phone                string  `json:"phone,omitempty"`
	Repinned             bool    `json:"repinned"`
}

type ErrorResponse struct {
	Errors []Error `json:"errors"`
}

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func getEvents(key string) []Event {
	u, err := url.Parse(UPCOMING_EVENTS_URL)
	if err != nil {
		log.Fatal(err)
	}
	q := u.Query()

	// https://www.meetup.com/meetup_api/auth/#keys
	q.Set("sign", "true")
	q.Set("key", key)

	// https://www.meetup.com/meetup_api/docs/find/upcoming_events/
	q.Set("order", ORDER)
	q.Set("topic_category", TOPIC_CATEGORY_TECH)
	q.Set("page", MAX_PAGE)
	q.Set("photo-host", PHOTO_HOST)

	u.RawQuery = q.Encode()

	res, err := http.Get(u.String())
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode != 200 {
		var r ErrorResponse
		json.Unmarshal(body, &r)
		for _, e := range r.Errors {
			log.Printf("%s: %s\n", e.Code, e.Message)
		}
		os.Exit(1)
	}

	var r Response
	err = json.Unmarshal(body, &r)
	if err != nil {
		log.Fatal(err)
	}

	return r.Events

}

func getFeed(events []Event) *feeds.Feed {
	feed := &feeds.Feed{
		Title:       FEED_TITLE,
		Link:        &feeds.Link{Href: FEED_URL},
		Description: FEED_DESCRIPTION,
		// Author: &feeds.Author{Name: FEED_NAME, Email: FEED_EMAIL},
		Created: time.Now(),
		// Copyright: FEED_COPYRIGHT,
	}

	for _, e := range events {
		item := &feeds.Item{
			Title:       e.Name,
			Link:        &feeds.Link{Href: e.Link},
			Description: e.Description,
			Author:      &feeds.Author{Name: e.Group.Name},
			Created:     e.Created.Time(),
			Updated:     e.Updated.Time(),
		}
		feed.Add(item)
	}

	return feed
}

func outputAtom(feed *feeds.Feed) {
	file, err := os.Create(FEED_OUTPUT)
	if err != nil {
		log.Fatal(err)
	}
	err = feed.WriteAtom(file)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	api_key := os.Getenv("MEETUP_API_KEY")
	events := getEvents(api_key)
	feed := getFeed(events)
	outputAtom(feed)
}
